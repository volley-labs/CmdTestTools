# Lightweight command line tools for automated testing

_Yet another one!?!_ **Yes!** One day of searching for such a tool lead to two days of implementing one. This tool is supposed to address a couple of simple requirements:

> Enable executing external programs (or BAT commands) and verifying the result is the expected one. Allow definining expectations on the standard output and any output files. All this packaged in a small self-contained executable (i.e. no external dependencies besides .NET Framework 3.5).

The `Release` version of `CmdRunTests.exe` is currently **22KB**. It relies only on assemblies provided by _.NET Framework 3.5_. Another important aspect is that expectation(s) for given output file(s) to have certain content is not by additionally providing the correct _file(s)_, but by providing the _hash_ value of the expected content. So, all you need is:

- The tool's binary (`CmdRunTests.exe`).
- The tasks definition file with all the tests to be performed, and expectations to be met, inside.

The format of this ...

## Configuring what to run + verifying the expected results

*NOTE:* The sample JSON configuration below is SSIS-related (`dtexec.exe` is used to execute SSIS packages):

```json
[
  {
    "name": "Preparation task",
    "prepare": "if not exist \"C:\\TestsOutput\\\" mkdir C:\\TestsOutput",
    "cleanup": "del /Q C:\\TestsOutput\\*",
    "wait":  true
  }, 
  {
    "name": "Simple encrypt & decrypt",
    "exe": "dtexec.exe",
    "arguments": "/proj S:\\TestPackages\\Packages\\{{SQL}}.Deployment\\StreamingData.ispac /package EncryptDecrypt.dtsx",
    "cleanup": "del /Q C:\\TestsOutput\\Zipped\\* && del /Q C:\\TestsOutput\\thyroid_out.zip",
    "expectations": [
      {
        "file": "C:\\TestsOutput\\thyroid_out.zip",
        "size": "55KB"
      },
      {
        "exit_code": 0,
        "output": "[OK]"
      },
      {
        "file": "C:\\TestsOutput\\Zipped\\thyroid_train.csv",
        "SHA1": "LTIsRMT/I2+U3ULJUBNZXmKsKhs=",
        "size": "275519"
      }
    ],
    "stdout_parsers": [ "dtexec_ok", "dtexec_failed", "cleaner" ],
    "stderr_parsers": [ "dtexec_ok", "dtexec_failed", "cleaner" ],
    "report": "default"
  },
  { /* Task #3 */ },
  { /* Task #4 */ },
  /* ... */
  { /* Task #N */ }
]
```

Most of the configurations sections are hopefully self-explanatory. One tricky thing is the use of _hashing_ instead of direct byte-per-byte comparison (`SHA1` field in one of the expectations). This spares the need to process large files in the tests (`thyroid_train.csv` in the example is a somewhat big file).

Another interesting concept are:

## Parsers

Sometimes programs' outputs are quite verbose and thus inconvenient to compare directly. But we can extract the interseting data and set expectations for it. Thus, the idea of having standard set of parsers and just specify which and in what order to be applied to the `stderr` and `stdout` streams. A parsers/formatters configuration file looks like this:

```json
{
  "parsers": [
    {
      "name": "dtexec_ok",
      "regex": "/.*DTSER_SUCCESS \\(0\\).*/[OK]/",
      "multilined": false
    },
    {
      "name": "dtexec_failed",
      "regex": "/.*DTSER_FAILURE \\(([0-9]+)\\).*/[Err: $1]/",
      "multilined": false
    }
  ],
  "formatters": [
    {
      "name": "gitlab",
      "OK": "{{task.name}}: [OK]",
      "Failure": "{{task.name}}: [Err]\n{{explanation}}"
    }
  ]
}
```

For a list of onging Issues / ToDo list, please check the... Issues section of the project.

That's basically it! Enjoy!

Ivan (Jonan) Georgiev @ Volley Labs Ltd. (subsidiary of COZYROC LLC)
