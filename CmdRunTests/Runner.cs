﻿/**
 * A simple wrapping class for runnig a task.
 * 
 * Copyright (C) 2017, Volley Labs Ltd.
 * Author: Ivan (Jonan) Georgiev
 */
using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;

namespace CmdTestTools
{
    public enum RunnerMode
    {
        Synchronous = 0,
        Asynchronous = 1,
        Default = Asynchronous
    };

    class Runner
    {
        private Process _process;
        private string _providedInput;
        private const int DEFAULT_TIMEOUT_MS = 5000;

        private AutoResetEvent _outputDoneEvent;
        private AutoResetEvent _errorDoneEvent;

        /// <summary>
        /// A process done callback prototype.
        /// </summary>
        /// <param name="runner">The runner handling the process that has just finished.</param>
        /// <param name="killed">A flag, showing whether the process was killed (like because of timeout), or finished naturally.</param>
        public delegate void FinishedEvent(Runner runner, bool killed);

        /// <summary>
        /// A callback prototype for when something arrives on the standard output (or error).
        /// </summary>
        /// <param name="runner">The runner handling the process, which produced the output</param>
        /// <param name="message">The actual message that arrived.</param>
        /// <param name="isError">A flag, showing if this is on standard output (false) or error (true).</param>
        public delegate void OutputEvent(Runner runner, string message, bool isError);

        #region Runner setup and management

        /// <summary>
        /// Initializes a task runner, without starting it.
        /// </summary>
        /// <param name="executable">The executable to be invoked</param>
        /// <param name="arguments">Arguments to be passed to the executable - as a single string.</param>
        /// <param name="mode">Synchronous or asynchronous mode to be used.</param>
        /// <remarks>When asynchrnous mode is used, the `OnFinised` delegate should be set, because this is
        /// the only way the result could be reported.</remarks>
        public Runner(string executable, string arguments = null, RunnerMode mode = RunnerMode.Asynchronous)
        {
            this.Mode = mode;
            this.Timeout = new TimeSpan(0, 0, 0, DEFAULT_TIMEOUT_MS / 1000, DEFAULT_TIMEOUT_MS % 1000);
            _process = new Process();
            _process.StartInfo.FileName = executable;
            _process.StartInfo.Arguments = arguments;
            _process.StartInfo.UseShellExecute = false;
            _process.StartInfo.CreateNoWindow = true;
            _process.StartInfo.WorkingDirectory = Directory.GetCurrentDirectory();
            _process.EnableRaisingEvents = true;

            _outputDoneEvent = new AutoResetEvent(false);
            _errorDoneEvent = new AutoResetEvent(false);
        }

        /// <summary>
        /// Starts the task in the selected mode, output redirection, input provisioning, etc.
        /// </summary>
        /// <returns>Whether the process has exited normally. Always <code>false</code> for asynchronous mode.</returns>
        /// <remarks>Will return immediately, if the mode is asynchronous.</remarks>
        public bool Start()
        {
            _process.Start();
            
            // Need to setup input provisioning.
            if (_providedInput != null)
                _process.StandardInput.Write(_providedInput);

            // If we have stream handling setup - initiate linie reading
            if (this.OnTextStream != null)
            {
                _process.BeginErrorReadLine();
                _process.BeginOutputReadLine();
            }

            // Now we can decide how to run it, based on the running mode
            bool exited;
            if (this.Mode == RunnerMode.Asynchronous && !_process.HasExited)
            {
                Debug.Assert(this.OnFinished != null);
                _process.Exited += new EventHandler((sender, e) => ReportFinishExecution(false));
                exited = false;
            }
            else // run in synchronous manner
            {
                exited = _process.WaitForExit((int)this.Timeout.TotalMilliseconds);
                if (!exited)
                    _process.Kill();

                ReportFinishExecution(!exited);
            }
            return exited;
        }

        /// <summary>
        /// Kills the process, if it is still running.
        /// </summary>
        public void Kill()
        {
            _process.Kill();
        }

        /// <summary>
        /// The callback for when the process is done. It _must_ be set in asynchronous mode, 
        /// and it _may_ be set, and will be invoked, in synchronous mode.
        /// </summary>
        public FinishedEvent OnFinished
        { get; set; }

        public OutputEvent OnTextStream
        { get; set; }

        #endregion

        #region Input/Output/Error properties and setters

        /// <summary>
        /// Sets the process input to a given string.
        /// </summary>
        /// <param name="input">The string to be sent to the process after it is started.</param>
        public void SetInput(string input)
        {
            // Check that we haven't been started yet.
            _process.StartInfo.RedirectStandardInput = true;
            _providedInput = input;
        }

        /// <summary>
        /// Setups the output (and error) redirection of the process. The `Output` and `Error` become
        /// usable after that.
        /// </summary>
        /// <param name="encoding">The encoding of the output streams.</param>
        public void SetOutputRedirect(Encoding encoding)
        {
            _process.StartInfo.RedirectStandardError = _process.StartInfo.RedirectStandardOutput = true;
            _process.StartInfo.StandardErrorEncoding = _process.StartInfo.StandardOutputEncoding = encoding;
            if (this.OnTextStream != null)
            {
                _process.OutputDataReceived += new DataReceivedEventHandler((sender, e) =>
                {
                    if (e.Data == null)
                        _outputDoneEvent.Set();
                    else
                        this.OnTextStream.Invoke(this, e.Data, false);
                });
                _process.ErrorDataReceived += new DataReceivedEventHandler((sender, e) =>
                {
                    if (e.Data == null)
                        _errorDoneEvent.Set();
                    else
                        this.OnTextStream.Invoke(this, e.Data, true);
                });
            }
        }

        /// <summary>
        /// The working directory the process to be invoked in.
        /// </summary>
        public string WorkingDirectory => _process.StartInfo.WorkingDirectory;

        /// <summary>
        /// The standard output stream of the processs, if the redirection was set.
        /// </summary>
        public StreamReader Output => _process.StandardOutput;

        /// <summary>
        /// The standard error stream of the process, if the redirection was set.
        /// </summary>
        public StreamReader Error => _process.StandardError;

        #endregion 

        #region Process execution properties

        /// <summary>
        /// The TimeSpan of execution (only uset processor time, no system).
        /// </summary>
        public TimeSpan Duration => _process.UserProcessorTime;

        /// <summary>
        /// The exit code of the process. Meaningful only, after the process has finished.
        /// </summary>
        public int ExitCode => _process.ExitCode;

        /// <summary>
        /// The timeout to wait for the process to finish, if synchronous mode is selected.
        /// </summary>
        public TimeSpan Timeout
        { get; set; }

        /// <summary>
        /// The currently selected running mode.
        /// </summary>
        public RunnerMode Mode
        { get; private set; }

        /// <summary>
        /// Additional information that is passed upon process finishing.
        /// </summary>
        public object UserInfo
        { get; set; }

        /// <summary>
        /// Whether the process is currently running.
        /// </summary>
        public bool IsRunning => _process.HasExited;
        #endregion

        #region Private methods
        private void ReportFinishExecution(bool killed)
        {
            WaitHandle.WaitAll(new WaitHandle[] { _errorDoneEvent, _outputDoneEvent }, (int)this.Timeout.TotalMilliseconds);
            _errorDoneEvent.Close();
            _outputDoneEvent.Close();
            this.OnFinished?.Invoke(this, killed);
        }

        #endregion

    }
}
