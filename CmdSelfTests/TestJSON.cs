using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Text;
using CmdTestTools;
using System.Collections.Generic;


namespace CmdSelfTests
{
    [TestClass]
    public class TaskJsonTest
    {
#if DEBUG // Assumes execution on windows dev environment
        private static string TASK_EXE = @"powershell.exe";
        private static string TASK_SIMPLE_OK = "OK\r\n";
#else // Assumes execution in the GitLab pipeline, i.e. Linux docker w/ .NET and (Linux) Powershell
        private static string TASK_EXE = @"pwsh";
        private static string TASK_SIMPLE_OK = "OK\n";
#endif

        private static string TASK_PARSED_OK = @"[OK]";

        private static string TASK_START_DEF = @"[{{
    ""name"": ""Simple PowerShell Tests"",
    ""exe"": ""{{{{Tool}}}}"",
    ""arguments"": ""-Command {0}"",
    ""expectations"": [
      {{
        ""exit_code"": {1},
        ""output"": ""{{{{Output}}}}""
      }}
    ],";

        private static string TASK_DTPARSERS_DEF = @"
    ""stdout_parsers"": [ ""ok_simplifier"", ""cleaner"" ],
    ""stderr_parsers"": [ ""ok_simplifier"", ""cleaner"" ],";

        private static string TASK_END_DEF = @"
    ""report"": ""default""
  }]";

        private static string TASK_PARSERS_DEF = @"{
      ""parsers"": [
        {
          ""name"": ""ok_simplifier"",
          ""regex"": ""/.*OK.*/[OK]/"",
          ""multilined"": false
        }
      ]}";

        private Stream BuildTaskDefinition(string script, int exit_code, string parsers = null)
        {
            StringBuilder combined = new StringBuilder();
            combined.AppendFormat(TASK_START_DEF, script, exit_code);
            if (parsers != null)
                combined.Append(parsers);
            combined.Append(TASK_END_DEF);

            return new MemoryStream(Encoding.UTF8.GetBytes(combined.ToString()));
        }

        [DataTestMethod]
        [DataRow("echo 'OK'; sleep 2", 0, 0)]
        [DataRow("echo 'OK'; sleep 2; exit 3", 3, 0)]
        [DataRow("echo 'Failure Test'; sleep 2", 0, 1)]
        public void SimpleRun(string script, int exit_code, int result)
        {
            CmdTesterProgram prg = new CmdTesterProgram(
                new Dictionary<string, string>() { { "{{Tool}}", TASK_EXE }, { "{{Output}}", TASK_SIMPLE_OK } }, 
                BuildTaskDefinition(script, exit_code), 
                null, 
                5, 
                true, 
                false);
            Assert.AreEqual(result, prg.Result);
        }

        [DataTestMethod]
        [DataRow(@"echo 'Some lines...\r\n... and words around OK Test'; sleep 2", 0, 0)]
        public void ParsingRun(string script, int exit_code, int result)
        {
            CmdTesterProgram prg = new CmdTesterProgram(
                new Dictionary<string, string>() { { "{{Tool}}", TASK_EXE }, { "{{Output}}", TASK_PARSED_OK } },
                BuildTaskDefinition(script, exit_code, TASK_DTPARSERS_DEF), 
                new MemoryStream(Encoding.UTF8.GetBytes(TASK_PARSERS_DEF)), 
                5, 
                true, 
                false);
            Assert.AreEqual(0, prg.Result);
        }

    }
}
